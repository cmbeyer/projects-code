import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class Alegeus {
	
	/**
	 * the library that we are running this code in
	 */
	private static String LIB="HTHDATV1";
			
	/**
	 * the client that we are running this code for
	 */
	//private static String CLIENT="FC1";
 	/**
 	 * Property file that has DB information
 	 */
 	private static Properties props;
 	/**
 	 * DB connection var to be used throughout the whole code
 	 */
 	private static Connection conn = null;
	
	/**
	 * This program reads in the (MTILCLAIMREQUEST.mbi) file and finds the 2 date fields and sets
	 * them to yesterday's date, then writes the new contents back to the original file.
	 */

	public static void main(String[] args) {	
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");//the output date format
		Calendar cal = Calendar.getInstance();//today's date
		cal.add(Calendar.DATE, -1);//subtract one day
		final String yesterday = dateFormat.format(cal.getTime());//save date
		
		//Reads file to check to match sure there is more then 1 line
				String FILENAME="/java/email/Alegeus/MTILCLAIMREQUEST.mbi";
				BufferedReader br = null;
				
				FileReader fr = null;
				try {

					br = new BufferedReader(new FileReader(FILENAME));//setting up to read the file in
					fr = new FileReader(FILENAME);
					br = new BufferedReader(fr);
					
					
					String sCurrentLine;
					StringBuilder sb=new StringBuilder();//string builder for the final output string
					int lineCount=0;
					while ((sCurrentLine = br.readLine()) != null) {//reads line by line
						lineCount++;
						
						if (lineCount==2) {//if second line
							sb.append(sCurrentLine.substring(0, 16)+yesterday+yesterday+sCurrentLine.substring(32)+"\n");//replaces the date
						}else {
							sb.append(sCurrentLine+"\n");//the rest of the lines
						}
					}
					
					System.out.println(sb.toString());
					
					
					PrintWriter writer = new PrintWriter(FILENAME);//rewriting to the file
		   			writer.print(sb.toString());
		   		    writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				props = new Properties();//get DB info
		 		try {
		 			props.load(new FileInputStream("/java/mydb2.properties"));
		 		} catch (FileNotFoundException e1) {
		 		e1.printStackTrace();
		 		} catch (IOException e1) {
		 			e1.printStackTrace();
		 		}
		 		
		 		String DRIVER = "com.ibm.as400.access.AS400JDBCDriver"; 
		 		String URL = "jdbc:as400://" + props.getProperty("local_system").trim() + "/"+LIB+";naming=system;errors=full";//jdbc:db2
		 		
		     	 try {
		 			 Class.forName(DRIVER); 
		 			 conn = DriverManager.getConnection(URL, props.getProperty("userId").trim(), props.getProperty("password").trim());
		     	
		 			DBinsert("INSERT INTO ALGFTPINF (PRCSEQ, OUTNM) VALUES ('3', '"+FILENAME+"')");//inserts the DB entry for later use
		     	 } catch (ClassNotFoundException e) {
		 			e.printStackTrace();
		 		} catch (SQLException e) {
		 			e.printStackTrace();
		 		}
	}	
	
	private static void DBinsert(String query){	
	 	Statement stmt = null;
					
    	try {
			 stmt = conn.createStatement();
			 stmt.execute(query);
			 stmt.close();
		} catch (SQLException e) {	
			System.out.println(e.getMessage());
		}
    }
}

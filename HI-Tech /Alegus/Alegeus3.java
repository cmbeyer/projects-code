import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Alegeus3 {

	/**
	 * the library that we are running this code in
	 */
	private static String LIB="HTHDATV1";

	/**
	 * the client that we are running this code for
	 */
	//private static String CLIENT="FC1";
	/**
	 * Property file that has DB information
	 */
	private static Properties props;
	/**
	 * DB connection var to be used throughout the whole code
	 */
	private static Connection conn = null;
	/**
	 * Sending email User name
	 */
	private static final String Email_UID = "no-reply";
	/**
	 * Sending email password 
	 */
	private static final String Email_PWD = "Hitech1990";
	/**
	 * the saved time that the program started to be used later in the elapse time
	 */
	private static long startTime;
	
//	private static Logger LOGGER=null;
	

	/**
	 * This program will read in the 1 record from the ALGFTPINF table, get the sent file and the recieved file and check to 
	 * see if the 2 files have the same number of record lines
	 */

	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception{
		startTime = System.currentTimeMillis();
		int lineCount=0, lineCount2=0;
		String path="";
		
	//	LOGGER = new MyLogger("ALEGEUS_P3","/java/logs/AlegeusP3Log.html", null);
	//	LOGGER=MyLogger.getLogger(MyLogger.GLOBAL_LOGGER_NAME);		

		props = new Properties();//get DB info
		try {
			props.load(new FileInputStream("/java/mydb2.properties"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();                           
		}
		
		String DRIVER = "com.ibm.as400.access.AS400JDBCDriver"; 
		String URL = "jdbc:as400://" + props.getProperty("local_system").trim() + "/"+LIB+";naming=system;errors=full";//jdbc:db2

		try {
			Class.forName(DRIVER); 
			conn = DriverManager.getConnection(URL, props.getProperty("userId").trim(), props.getProperty("password").trim()); 

			//DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM) values ('1', '/java/email/Alegeus/MTCLM120417154548.mbi')");
			//System.out.println("ADDING FAKE DATA");
			Statement stmt = conn.createStatement();//connect to DB and setup Aliases

			if (args[0].equals("1")) {//the first alegeus process
				if (DBqueryINT("SELECT COUNT(*) FROM ALGFTPINF WHERE PRCSEQ ='1' AND OUTNM <> 'out' AND OUTNM <> 'in'")>0) {//tests to see if the file was sent in the first place
					ResultSet rs = stmt.executeQuery("SELECT * FROM ALGFTPINF WHERE PRCSEQ ='1' AND OUTNM <> 'out' AND OUTNM <> 'in'");
					rs.next();

					//SET FILE
					// Open the file
					FileInputStream fstream = new FileInputStream(rs.getString("OUTNM").trim());
					BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
					br.readLine();//reads first line
					
					String strLine;

					//Read File Line By Line
					while ((strLine = br.readLine()) != null)   {
						lineCount++;
					}

					//Close the input stream
					br.close();	

					//saves the count of out lines
					DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('1', 'out','"+lineCount+"')");
					System.out.println("p1_out found");

					//RECIEVED FILE
					File folder = new File(rs.getString("OUTNM").trim().substring(0, rs.getString("OUTNM").trim().indexOf("."))+".res");//"/java/email/Alegeus");
					//File[] listOfFiles = folder.listFiles();
					System.out.println(folder.getPath());
					//if (listOfFiles.length<=0) {//ends if no files are found
					if (!folder.exists()) {
						System.out.println("p1_in NOT found");
						exitFun(args[0]);
					}

					path=folder.getPath();//listOfFiles[0].getPath();
					DateFormat dateFormat = new SimpleDateFormat("MMddyy");
					Date d=Calendar.getInstance().getTime();

					//for (int i = 0; i < listOfFiles.length; i++) {
					//	if (listOfFiles[i].getName().contains(dateFormat.format(d)))
					//		System.out.println("File " + listOfFiles[i].getName());
					//}

					// Open the file
					fstream = new FileInputStream(path);
					br = new BufferedReader(new InputStreamReader(fstream));
					br.readLine();//reads first line
					
					//Read File Line By Line
					while ((strLine = br.readLine()) != null)   {
						lineCount2++;
						//LOGGER.info(strLine);
					}

					//Close the input stream
					br.close();	
					rs.close();

					//saves the count of out lines
					//LOGGER.config("Total records processed"+lineCount2);
					DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('1', 'in','"+lineCount2+"')");
					System.out.println("p1_in found");
				}else {
					DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('1', 'out','0')");
					DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('1', 'in','0')");
					System.out.println("p1_in NOT found DB set to 0");
					System.exit(0);
				}
			} else {//the 3rd alegeus process
				
				
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");//the output date format
				
				Calendar cal = Calendar.getInstance();//today's date
				cal.add(Calendar.DATE, -1);//subtract one day
				String yesterday = dateFormat.format(cal.getTime());//save date
				
				File folder = new File("/java/email/Alegeus/MTILCLAIMREQUEST_"+yesterday+".exp");
				copyFileUsingChannel();
				//File[] listOfFiles = folder.listFiles();

				//if (listOfFiles.length<=0) {//ends if no files are found
				
				if (!folder.exists()) {
					System.out.println("p3_in NOT found");
					exitFun(args[0]);
				}

				// Open the file
				path= folder.getPath(); //listOfFiles[0].getPath();
				FileInputStream fstream = new FileInputStream(path);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
				br.readLine();
				
				//Read File Line By Line
				String strLine="";
				while ((strLine = br.readLine()) != null)   {
					lineCount2++;
					
				}

				//Close the input stream
				br.close();	

				//saves the count of out lines
				DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('3', 'in','"+lineCount2+"')");
				System.out.println("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('3', 'in','"+lineCount2+"')");
				System.out.println("p3_in found");
			}

			DBupdate("UPDATE ALGFTPINF SET INNM='"+path+"' WHERE PRCSEQ ='"+args[0]+"'");
			System.out.print("UPDATE ALGFTPINF SET INNM='"+path+"' WHERE PRCSEQ ='"+args[0]+"'");

			stmt.close();

			if (args[0].equals("3")) { 
				Send(Email_UID,Email_PWD,"jwalsh@hi-techhealth.com,twalsh@hi-techhealth.com,TMcKelvey@bacwv.com,LDenny@bacwv.com","", //LDenny@bacwv.com,TMcKelvey@bacwv.com
				//Send(Email_UID,Email_PWD,"jpatriarca@hi-techhealth.com","",
						"ALEGEUS Process","<table>" +
								"<tr><th>Process</th><th style=\"padding:20px;\">Total Sent</th><th style=\\\"padding:20px;\\\">Total Received</th></tr>" +
								"<tr><td style=\"padding:0px 20px 0px 20px;\">P1: Daily Manual Claim Build</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '1' and OUTNM = 'out'")+"</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '1' and OUTNM = 'in'")+"</td></tr>" +
								"<tr><td style=\"padding:0px 20px 0px 20px;\">P2: Daily Eligibility Update Build</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '2' and OUTNM = 'in'")+"</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">-</td></tr>" +
								"<tr><td style=\"padding:0px 20px 0px 20px;\">P3: Daily Card Swipe Claim Build</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">0</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '3' and OUTNM = 'in'")+"</td></tr>" +
						"</table>");

				DBupdate("DELETE FROM ALGFTPINF WHERE PRCSEQ ='1' and OUTNM ='in'");
				DBupdate("DELETE FROM ALGFTPINF WHERE PRCSEQ ='2' and OUTNM ='in'");
				DBupdate("DELETE FROM ALGFTPINF WHERE PRCSEQ ='1' and OUTNM ='out'");
				DBupdate("DELETE FROM ALGFTPINF WHERE PRCSEQ ='3' and OUTNM ='in'");

				System.out.println("p3 good email sent");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//emailReports.store(output, null);
		//output.close();
		System.out.println("good save");
		System.out.println("\n\nDONE in: " + ((System.currentTimeMillis()-startTime)/ 1000)+"secs\n\n\n");
		conn.close();
	}
	
	@SuppressWarnings("resource")
	private static void copyFileUsingChannel() {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");//the output date format
		
		Calendar cal = Calendar.getInstance();//today's date
		
		cal.add(Calendar.DATE, -1);//subtract one day
		
		String yesterday = dateFormat.format(cal.getTime());//save date
		
		
		File source = new File("/java/email/Alegeus/MTILCLAIMREQUEST_"+yesterday+".exp");
		File dest = new File("/java/email/Alegeus/MTILCLAIMREQUEST_"+new SimpleDateFormat("MMddyyyy_hhmmss").format(new Date())+".exp");

		
	    FileChannel sourceChannel = null;
	    FileChannel destChannel = null;
	    try {
	        sourceChannel = new FileInputStream(source).getChannel();
	        destChannel = new FileOutputStream(dest).getChannel();
	        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	       } catch (IOException e) {
			e.printStackTrace();
		}finally{
	           try {
				sourceChannel.close();
		        destChannel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	   }
	}

	/**
	 * Checks to see how many times this code ran today and updates the count if its the first time
	 * then exits. 2nd time it will send an email then exit and delete the table entry
	 */
	private static void exitFun(String i) {//2nd round
		if (DBqueryINT("SELECT COUNT FROM ALGFTPINF WHERE PRCSEQ ='"+i+"'")==1) {
			if (i.equals("3")) {
				Send(Email_UID,Email_PWD,"cbeyer@hi-techhealth.com,twalsh@hi-techhealth.com,frivera@hi-techhealth.com,TMcKelvey@bacwv.com,LDenny@bacwv.com","",
				//Send(Email_UID,Email_PWD,"jpatriarca@hi-techhealth.com","",
								"ALEGEUS Process","<table>" +
								"<tr><th>Process</th><th style=\"padding:20px;\">Total Sent</th><th style=\\\"padding:20px;\\\">Total Received</th></tr>" +
								"<tr><td style=\"padding:0px 20px 0px 20px;\">P1: Daily Manual Claim Build</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '1' and OUTNM = 'out'")+"</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '1' and OUTNM = 'in'")+"</td></tr>" +
								"<tr><td style=\"padding:0px 20px 0px 20px;\">P2: Daily Eligibility Update Build</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '2' and OUTNM = 'in'")+"</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">-</td></tr>" +
								"<tr><td style=\"padding:0px 20px 0px 20px;\">P3: Daily Card Swipe Claim Build</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">-</td><td style=\"padding:0px 20px 0px 20px;\" align=\"right\">"+DBqueryINT("select COUNT from ALGFTPINF where PRCSEQ = '3' and OUTNM = 'in'")+"</td></tr>" +
						"</table>");

				System.out.println("DELETE FROM ALGFTPINF WHERE PRCSEQ ='"+i+"'");
				DBupdate("DELETE FROM ALGFTPINF WHERE PRCSEQ ='1'");
				DBupdate("DELETE FROM ALGFTPINF WHERE PRCSEQ ='3'");

				System.out.println("p3 bad email sent");
			}
		}else {//1st round
			System.out.println("UPDATE ALGFTPINF SET COUNT='1' WHERE PRCSEQ ='"+i+"'");
			DBupdate("UPDATE ALGFTPINF SET COUNT='1' WHERE PRCSEQ ='"+i+"'");
			
			if (i.equals("3"))
				DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('3', 'in','0')");
			else 
				DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('1', 'in','0')");
		}

		System.out.println("p3 bad save");
		System.exit(0);
	}

	/**
	 * Queries the DB using the giving query string, only returns a int not result set
	 * 
	 * @param query Query string such as "Select *"
	 * @return first search result integer
	 */
	private static int DBqueryINT(String query){
		int result=0;
		try {
			Statement stmt1 = conn.createStatement();
			ResultSet rs = stmt1.executeQuery(query);

			rs.next();
			result = rs.getInt(1);

			rs.close();		
			stmt1.close();
		} catch (SQLException e) {e.printStackTrace();}
		return result;
	}

	/**
	 * Queries the DB using the giving query string
	 * 
	 * @param query Query string such as "insert *" or "update *"
	 */
	private static void DBupdate(String query){	
		Statement stmt = null;

		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	/**
	 * Sends an email using the given parms and will attach a file if passed True
	 * 
	 * @param username email user name
	 * @param password email password
	 * @param recipientEmail all the email addresses you want to send to separated by ,
	 * @param ccEmail all the email addresses you want to BCC to separated by ,
	 * @param title the email subject line
	 * @param body the email body
	 * @param attachment if we should send predefined attachment file
	 */
	private static void Send(final String username, final String password, String recipientEmail, String ccEmail, String title, String body){
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.office365.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.debug", "false");

		Session session = Session.getInstance(props,
				new Authenticator(){
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("no-reply@hi-techhealth.com", "Poker*Star19");}
		});

		try {
			MimeMessage msg = new MimeMessage(session);

			//set from address
			msg.setFrom(new InternetAddress("support@hi-techhealth.com"));

			//set to address
			String[] recipientList = recipientEmail.split(",");
			InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
			int counter = 0;
			for (String recipient : recipientList) {
				recipientAddress[counter] = new InternetAddress(recipient.trim());
				counter++;
			}
			msg.setRecipients(Message.RecipientType.TO, recipientAddress);

			//set CC address
			if (ccEmail.length() > 0) {
				String[] CCList = ccEmail.split(",");
				InternetAddress[] CCAddress = new InternetAddress[CCList.length];
				int c = 0;
				for (String CC : CCList) {
					CCAddress[c] = new InternetAddress(CC.trim());
					c++;
				}
				msg.setRecipients(Message.RecipientType.CC, CCAddress);
			}

			msg.setSubject(title);
			msg.setSentDate(new Date());

			//set contents
			msg.setText(body, "utf-8", "html");

			Transport.send(msg);

			//System.out.println("EMAIL SENT");

		}
		catch (MessagingException mex) {mex.printStackTrace();}    
	} 

}

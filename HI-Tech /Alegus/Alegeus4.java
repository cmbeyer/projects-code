import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Alegeus4 {
	
	/**
	 * the library that we are running this code in
	 */
	private static String LIB="HTHDATV1";
	/**
	 * DB connection var to be used throughout the whole code
	 */
	private static Connection conn = null;
	

	public static void main(String[] args) {
		Properties props = new Properties();//get DB info
		try {
			props.load(new FileInputStream("/java/mydb2.properties"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();                           
		}


		String DRIVER = "com.ibm.as400.access.AS400JDBCDriver"; 
		String URL = "jdbc:as400://" + props.getProperty("local_system").trim() + "/"+LIB+";naming=system;errors=full";//jdbc:db2

		try {
			Class.forName(DRIVER); 
			conn = DriverManager.getConnection(URL, props.getProperty("userId").trim(), props.getProperty("password").trim()); 
			
		//Reads file to check to match sure there is more then 1 line
		String FILENAME=args[0].trim();	
		
		System.out.println(FILENAME);
		
		BufferedReader br = new BufferedReader(new FileReader(FILENAME));//setting up to read the file in
		FileReader fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);
			
			String sCurrentLine="";
			int lineCount=0;
			while ((sCurrentLine = br.readLine()) != null) {//reads line by line
				lineCount++;
			}
			
			DBupdate("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('2', 'in','"+lineCount+"')");
			System.out.println("insert into ALGFTPINF (PRCSEQ, OUTNM, COUNT) values ('2', 'in','"+lineCount+"')");

			
			br.close();
			fr.close();
			conn.close();
		} catch (IOException e) {
			System.out.println ("NO FILE FOUND");
			//e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Queries the DB using the giving query string
	 * 
	 * @param query Query string such as "insert *" or "update *"
	 */
	private static void DBupdate(String query){	
		Statement stmt = null;

		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

}

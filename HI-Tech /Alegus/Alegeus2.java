import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Alegeus2 {
	
	/**
	 * This program reads a file and finds the first line and skips it then writes the new 
	 * contents back to the original file.
	 */

	public static void main(String[] args) {	
		//Reads file to check to match sure there is more then 1 line
				String FILENAME=args[0].trim();
				BufferedReader br = null;
				
				FileReader fr = null;
				try {

					br = new BufferedReader(new FileReader(FILENAME));//setting up to read the file in
					fr = new FileReader(FILENAME);
					br = new BufferedReader(fr);
					
					
					String sCurrentLine;
					StringBuilder sb=new StringBuilder();//string builder for the final output string
					int lineCount=0;
					while ((sCurrentLine = br.readLine()) != null) {//reads line by line
						lineCount++;
						
						if (lineCount!=1) {//skips first line
							sb.append(sCurrentLine+"\r");
						}
					}
					
					System.out.println(sb.toString());
					
					PrintWriter writer = new PrintWriter(FILENAME);//rewriting to the file
		   			writer.print(sb.toString());
		   		    writer.close();
				} catch (IOException e) {
					System.out.println ("NO FILE FOUND");
					//e.printStackTrace();
				}
	}	
}

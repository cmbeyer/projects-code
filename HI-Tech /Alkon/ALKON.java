import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.QSYSObjectPathName;




public class ALKON{

	private static StringBuilder finalString = new StringBuilder();

	/**
	 * the current library we are using 
	 */
	private static String LIB;
	/**
	 * the current client we are working on
	 */
	private static String CLIENT;
	/* The save location of the final output file 
	 */
	public static String finalFile="/java/email/alkon/ALKONFTP_OUT.txt";

	public SimpleDateFormat formatterOUT = new SimpleDateFormat("yyyyMMdd");

	public static int[] lengths=null;

	public static String QUERY;
	public static String NOTE;
	public static int count=0;
	public static String MEDPCR;
	public static String MEDACC;

	public static String QUERYFIRST;

	public static String QUERYOTHER;

	public String PROCESS="**NOT DEFINED**";

	private static Connection conn;
	
	static Properties props = new Properties();//get DB info
	/* date formatter for 4 digit years
	 */

	public final SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");

	/**
	 * date formatter for 2 digit years
	 */

	public final SimpleDateFormat formattershort = new SimpleDateFormat("MMddyy");

	public static void main (String args[]){  	 
		//get input from command line

		LIB="TESTDATA";
		CLIENT="BW1";

		new ALKON(LIB,CLIENT);
	}    

	/**
	 * Reads in the input file and saves each line of data to the line arraylist and validates
	 * some of the fields to make sure they are in the correct form.
	 */

	@SuppressWarnings("unused")
	public ALKON(String LIB, String CLIENT){

		String hostname="";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		}catch (UnknownHostException ex){
			System.out.println("Hostname can not be resolved");
			}
		

		File f = new File(finalFile);//tests to see if the does not exist already and tells it to get a new copy of the template first
		if (!f.exists())
			try {
				f.createNewFile();
			} catch (IOException e) {

				e.printStackTrace();
			}
		
		try {
			readFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//run();
}
	
	@SuppressWarnings("resource")
	public static void readFile() throws Exception{
		 Date date = new Date();
		 String today=new SimpleDateFormat("yyyyMMddHHmm").format(date);
		 System.out.println(today);
		String clmnbr="";
	    try {
	    	
	        // change this value
	    	finalString.append("                                                                "
	    			+ "                                                                            "
	    			+ "                                                            HDT"+today+"BAC                           MHS                           0000001T10"+"\n");
	    	
	        BufferedReader bufferreader = new BufferedReader(new FileReader("/java/email/alkon/ALKONFTP.TXT"));
	        String next, line = bufferreader.readLine();
	        
	        for (boolean first = true, last = (line == null); !last; first = false, line = next) {
                last = ((next = bufferreader.readLine()) == null);

                if (first) {
                	System.out.println("first line: " + line);
                } else if (last) {
                	System.out.println("last line: " + line);
                } else {
                	System.out.println("normal line: " + line);
                	String Old=line.substring(202,204).trim();
                	String header=line.substring(200,202);
                	System.out.println("header: "+header);
                	if(header.contains("TR") || header.contains("HD"))
                			{
                		count++;
                			}
                	else if(!Old.contains("DR"))
                	{
                		System.out.println("OLD:  "+"||"+Old+"||");
                		clmnbr = input(line);
                		run(clmnbr);
                		count++;
                	}
                	
                }
	        }
	        
	       /* while (line != null) {     
	          //do whatever here 
	            line = bufferreader.readLine();
	           clmnbr = input(line);
	           run(clmnbr);
	        }
	        */
	        
	        Finalwrite();
	        
	    }catch (FileNotFoundException ex) {
	        ex.printStackTrace();
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }

	}

	@SuppressWarnings("unused")
	public static void run(String clmnbr){

		if (conn==null)
			connect();

		System.currentTimeMillis();

		QUERYFIRST="SELECT * FROM QTEMP/CLMHDR_"+CLIENT+" JOIN QTEMP/CLMDET_"+CLIENT+" ON CLMHDR_"+CLIENT+".HCLMNO=CLMDET_"
				    +CLIENT+".DCLMNO WHERE HGRPNO='999999' AND HCLMNO='"+clmnbr+"'";

		
		try {
			
			ResultSet rs_claims = conn.createStatement().executeQuery(QUERYFIRST);
			
			int count=1, repeat=0;

			while(rs_claims.next()){
				repeat++;
				if (repeat%400==0){//reset the connections
					rs_claims.close();
					connect();
					rs_claims = conn.createStatement().executeQuery(QUERYFIRST); 

					for (int t=0; t<(400*count); t++){
						rs_claims.next();
					}

					count++;
					System.out.println("RESETING"+repeat);
				}
				
				String hssn=rs_claims.getString("HSSN");
				String hdiv=rs_claims.getString("HDIV");
				String hplan=rs_claims.getString("HPLAN");
				String RelCodeInbound = rs_claims.getString("HDEP");
				String hdep=rs_claims.getString("HDEP");
				ResultSet rs_insure = null;
				ResultSet rs_medpcr = null;
				ResultSet rs_clmnot = null;
				ResultSet rs_medacc = null;
				
				if(hdep.equals("EE")){

					QUERY="SELECT * FROM QTEMP/INSURE_"+CLIENT+" WHERE IGRPNO = '999999' AND ISSN='"+hssn+"'";
					MEDPCR="SELECT * FROM QTEMP/MEDPCR_"+CLIENT+" WHERE PGRPNO='999999' AND PDIV = '001' AND PPLAN ='PPO'";
					NOTE="SELECT * FROM QTEMP/CLMNOT_"+CLIENT+" WHERE NCLMNO = '"+clmnbr+"'";
					MEDACC="SELECT * FROM QTEMP/MEDACC_"+CLIENT+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and APLNPR='M' and AYEAR='18' and ACCDE='"+hdep+"'";
					
					rs_insure = conn.createStatement().executeQuery(QUERY); 
					rs_medpcr = conn.createStatement().executeQuery(MEDPCR);
					rs_clmnot=conn.createStatement().executeQuery(NOTE);
					rs_medacc=conn.createStatement().executeQuery(MEDACC);
					
					rs_insure.next();
					rs_medpcr.next();
					rs_clmnot.next();
					rs_medacc.next();
					output(rs_insure, rs_claims, rs_medpcr,rs_clmnot,rs_medacc, true);
				}
				else{
					if(hdep.length()!=2)
					{
						hdep="0"+hdep;
					}
					System.out.println("IN THE ELSE");
					MEDPCR="SELECT * FROM QTEMP/MEDPCR_"+CLIENT+" WHERE PGRPNO='999999' AND PDIV = '001' AND PPLAN ='PPO'";
							
					QUERYOTHER="SELECT * FROM QTEMP/INSDEP_"+CLIENT+" JOIN QTEMP/INSURE_"+CLIENT+" ON "
					+"INSDEP_"+CLIENT+".DESSN = INSURE_"+CLIENT+".ISSN "+"AND INSDEP_"+CLIENT+".DGRPNO = INSURE_"+CLIENT
					+".IGRPNO WHERE " + "DGRPNO = '999999' AND DESSN='"+hssn+"'";
					
					NOTE="SELECT * FROM QTEMP/CLMNOT_"+CLIENT+" WHERE NCLMNO = '"+clmnbr+"'";
					MEDACC="SELECT * FROM QTEMP/MEDACC_"+CLIENT+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and APLNPR='M' and AYEAR='18' and ACCDE='"+hdep+"'";
					
					rs_insure = conn.createStatement().executeQuery(QUERYOTHER); 
					rs_medpcr = conn.createStatement().executeQuery(MEDPCR);
					rs_clmnot=conn.createStatement().executeQuery(NOTE);
					rs_medacc=conn.createStatement().executeQuery(MEDACC);
					
					rs_insure.next();
					rs_medpcr.next();
					rs_clmnot.next();
					rs_medacc.next();
					
					output(rs_insure, rs_claims, rs_medpcr,rs_clmnot,rs_medacc, false);
				}

				System.out.println("Current count: "+repeat);
				
				rs_insure.close();	
				rs_medpcr.close();
				
			}
			
					 
			rs_claims.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}

	public static void Finalwrite()
	{
		
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(finalFile)));
			String stringcount=Integer.toString(count);
			
			if(stringcount.length()!=17){
				while(stringcount.length()!=17){
				stringcount="0"+stringcount;
				
				}
			}
			
			finalString.append("                                  "
					+ "                                           "
					+ "                                           "
					+ "                                           "
					+ "                                     TR"+stringcount);
			out.print(finalString.toString());
			System.out.println("WROTE OUTPUT FILE");
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}			
	}

public static String input(String line) throws ParseException{
	
	System.out.println("start Input");
	
	if (conn==null)
		connect();
		 
	System.out.println("After connect");
	
		 Date date = new Date();
		
		 SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd");
		 SimpleDateFormat outputFormat = new SimpleDateFormat("MMddyy");
		 
		 String expgrpno=line.substring(600,615).trim();
		 String hgrpno = "999999";  //line.substring(600,615).trim();
		 String provdr = "039332";
	     String hssn = line.substring(580,600).trim();
	     String relcode = line.substring(872,875).trim();
	     //String BrandGen = line.substring(1633,1633).trim();
	     //String DrugName = line.substring(1571,1589).trim();
	     String ProcessDate=new SimpleDateFormat("MMddyy").format(date);
	     String RecDate=new SimpleDateFormat("MMddyy").format(date);
	     String Analyst="CV"+new SimpleDateFormat("MMddyy").format(date);
	     String MedCoClmno=line.substring(480,510).trim();
	     String DOS=line.substring(315,323).trim();
	     String AccumQual1=line.substring(989,991).trim();
	     String AccumQual2=line.substring(1025,1027).trim();
	     String AccumNetwork1=line.substring(991,992).trim();
	     String AccumNetwork2=line.substring(1027,1028).trim();
	     String AccumApplied1=line.substring(992,1002).trim();
	     String AccumApplied2=line.substring(1028,1038).trim();
	     String AccumBenPeriod1=line.substring(1003,1013).trim();
	     String AccumBenPeriod2=line.substring(1039,1049).trim();
	     String Actioncode1=line.substring(1002,1003).trim();
	     String Actioncode2=line.substring(1013,1014).trim();
	     String planname=findplan("999999",hssn);
	     String clmnbr=CallCL();
	     
	     DOS=outputFormat.format(inputFormat.parse(DOS));
	     
	     if(relcode.equals("001"))
	     {
	    	relcode="EE";
	    	 
	     }
	     else if(relcode.equals("002"))
	     {
	    	 relcode="SP";
	     }
	     else if(!relcode.equals("001") && !relcode.equals("002")){
	    	 
	    	String changerelcode=relcode.substring(1, 3);
	    	int minusrelcode=Integer.parseInt(changerelcode);
	    	minusrelcode=minusrelcode-02;
	    	relcode=Integer.toString(minusrelcode);
	    	System.out.println("dependent:  "+relcode);
	    	
	     }
	     System.out.println("dependent:  "+relcode);
	     
	     if(relcode.length()!=2)
			{
				relcode="0"+relcode;
			}
	     
	     String amount =AccumApplied1.substring(0, AccumApplied1.length()-2)+"."+AccumApplied1.substring(AccumApplied1.length()-2);
	     String amount2 =AccumApplied2.substring(0, AccumApplied2.length()-2)+"."+AccumApplied2.substring(AccumApplied2.length()-2);
	     amount=StringUtils.stripStart(amount,"0");
	     amount2=StringUtils.stripStart(amount2,"0");
	     
	     double number=Double.parseDouble(amount);
	     double number2=Double.parseDouble(amount2);
	     
	     System.out.println("NUMBER: "+number+"  AMOUNT: "+amount);
	     if(Actioncode1.equals("-"))
	     {
	    	 number=number*-1;
	    	 amount= Double.toString(number);
	    }
	     if(Actioncode2.equals("-"))
	     {
	    	 number2=number2*-1;
	    	 amount2= Double.toString(number2);
	    }
	     
	     int medcheck;
	     
	     medcheck = DBqueryINT("SELECT COUNT(*) FROM QTEMP/MEDACC_"+CLIENT+" WHERE ASSN = '"+hssn+"' AND AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'");
	     ResultSet medacccheck = null;
	     
	     if(medcheck>0)
	     {
	    	 double OUTOFNET = 0;
			double INNET = 0;
			
			
			try {
				medacccheck=DBqueryRS("SELECT * FROM QTEMP/MEDACC_"+CLIENT+" WHERE ASSN = '"+hssn+"' AND AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'", medacccheck);
				 medacccheck.next();
				 
				 if(AccumQual1.equals("4"))
				 {
					 OUTOFNET = medacccheck.getDouble("OANNDE");
					 INNET = medacccheck.getDouble("AANNDE");
				 }
				 if(AccumQual1.equals("5"))
				 {
					 OUTOFNET = medacccheck.getDouble("ONETOP");
					 INNET = medacccheck.getDouble("ANETOP");
				 }
				 
				 
				 if(AccumQual2.equals("4"))
				 {
					 OUTOFNET = medacccheck.getDouble("OANNDE");
					 INNET = medacccheck.getDouble("AANNDE");
				 }
				 if(AccumQual2.equals("5"))
				 {
					 OUTOFNET = medacccheck.getDouble("ONETOP");
					 INNET = medacccheck.getDouble("ANETOP");
				 }
				 
			} catch (SQLException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
	    	 
	    	 if(AccumNetwork1.equals("1"))
	    	 {
	    		 INNET= INNET+ number;
	    	 }
	    	 
	    	 if(AccumNetwork1.equals("2"))
	    	 {
	    		 OUTOFNET= OUTOFNET+ number;
	    	 }
	    	 
	    	 if(AccumNetwork2.equals("1"))
	    	 {
	    		 INNET= INNET+ number2;
	    	 }
	    	 
	    	 if(AccumNetwork2.equals("2"))
	    	 {
	    		 OUTOFNET= OUTOFNET+ number2;
	    	 }
	    	 
	    	 if(AccumQual1.equals("04"))
			 {
	    		 System.out.println("UPDATE QTEMP/MEDACC_"+CLIENT+" set AANNDE="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'");
		    	 DBinsert("UPDATE QTEMP/MEDACC_"+CLIENT+" SET AANNDE="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'","MEDACC");
			 }
	    	 
	    	 if(AccumQual1.equals("05"))
			 {
	    		 System.out.println("UPDATE QTEMP/MEDACC_"+CLIENT+" set ANETOP="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'");
		    	 DBinsert("UPDATE QTEMP/MEDACC_"+CLIENT+" SET ANETOP="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'","MEDACC");
			 }
	    	 if(AccumQual2.equals("04"))
			 {
	    		 System.out.println("UPDATE QTEMP/MEDACC_"+CLIENT+" set AANNDE="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'");
		    	 DBinsert("UPDATE QTEMP/MEDACC_"+CLIENT+" SET AANNDE="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'","MEDACC");
			 }
	    	 
	    	 if(AccumQual2.equals("05"))
			 {
	    		 System.out.println("UPDATE QTEMP/MEDACC_"+CLIENT+" set ANETOP="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'");
		    	 DBinsert("UPDATE QTEMP/MEDACC_"+CLIENT+" SET ANETOP="+INNET+", OANNDE="+OUTOFNET+" WHERE AGRPNO='999999' AND ASSN='"+hssn+"' and AYEAR='18' and APLNPR='M' and ACCDE='"+relcode+"'","MEDACC");
			 }
	    	  
	    	  
	     }
	     else
	     {
	    	 double totalIN = 0;
	    	 double totalOON = 0;
	    	 
	    	 if(AccumNetwork1.equals("1"))
	    	 {
	    		 totalIN= totalIN+ number;
	    	 }
	    	 
	    	 if(AccumNetwork1.equals("2"))
	    	 {
	    		 totalOON= totalOON+ number;
	    	 }
	    	 
	    	 if(AccumNetwork2.equals("1"))
	    	 {
	    		 totalIN= totalIN+ number2;
	    	 }
	    	 
	    	 if(AccumNetwork2.equals("2"))
	    	 {
	    		 totalOON= totalOON+ number2;
	    	 }
	    	 
	    	 System.out.println("Insert into QTEMP/MEDACC_"+CLIENT+"(AANNDE,OANNDE,AGRPNO,ASSN,AYEAR,APLNPR,ACCDE) VALUES("+totalIN+","+totalOON+",'999999','"+hssn+"','18','M','"+relcode+"')");
	    	 
	    	 DBinsert("Insert into QTEMP/MEDACC_"+CLIENT+" (AANNDE,OANNDE,AGRPNO,ASSN,AYEAR,APLNPR,ACCDE) VALUES("+totalIN+","+totalOON+",'999999','"+hssn+"','18','M','"+relcode+"')","MEDACC");
	     }
	     
	     /*
	      System.out.println("AccumApplied1  "+AccumApplied1+"New Value: "+amount+"|| action code: "+Actioncode1+"|| clmnbr "+clmnbr);
	     
	     System.out.println("INSERT INTO QTEMP/CLMHDR_"+CLIENT+"(HCLMNO, HGRPNO, HSSN, HDEP, HPLAN, HPROVD, HRECDT, HPRODT, HANALY, HOCCCD, ,HDOS, HTOTDD) VALUES ('"+clmnbr+"','"+hgrpno+"','"+hssn+"','"+relcode+"','"+planname+"','"+provdr+"','"+RecDate+"','"+ProcessDate+"','"+Analyst+"','"+"99"+"','"+DOS+"','"+amount+"')");
	     System.out.println("INSERT INTO QTEMP/CLMDET_"+CLIENT+"(DCLMNO, DGRPNO, DDEP, DSSN, DDOS, DLINE, DAMTDD) VALUES ('"+clmnbr+"','"+hgrpno+"','"+relcode+"','"+hssn+"','"+DOS+"','001','"+amount+"')");
	     */
	    DBinsert("INSERT INTO QTEMP/CLMHDR_"+CLIENT+"(HCLMNO, HGRPNO, HSSN, HDEP, HPLAN, HPROVD, HRECDT, HPRODT, HANALY, HOCCCD, HDOS, HTOTDD, HFILL7) VALUES ('"+clmnbr+"','"+hgrpno+"','"+hssn+"','"+relcode+"','"+planname+"','"+provdr+"','"+RecDate+"','"+ProcessDate+"','"+Analyst+"','"+"99"+"','"+DOS+"','"+amount+"','M')","CLMHDR");
	    DBinsert("INSERT INTO QTEMP/CLMDET_"+CLIENT+"(DCLMNO, DGRPNO, DDEP, DSSN, DDOS, DLINE, DAMTDD) VALUES ('"+clmnbr+"','"+hgrpno+"','"+relcode+"','"+hssn+"','"+DOS+"','001','"+amount+"')","CLMDET");
	    DBinsert("INSERT INTO QTEMP/CLMNOT_"+CLIENT+"(NCLMNO,NCNOT1) VALUES ('"+clmnbr+"','"+expgrpno+"')","CLMNOT");
	    
	    return clmnbr;
}
	@SuppressWarnings("unused")
	public static String[] output(ResultSet rs_insure, ResultSet rs_claims, ResultSet rs_medpcr, ResultSet rs_clmnot,ResultSet rs_medacc, boolean isEE) throws SQLException {
		
		/*
		 * Adjusting Pre-Data
		 */
		String hssn=rs_claims.getString("HSSN");
		
		Date date = new Date();
		String TransmissionDate= new SimpleDateFormat("yyyyMMdd").format(date);

		String TransmissionTime = new SimpleDateFormat("HHmmssdd").format(date);
		String TransmissionID = TransmissionDate + TransmissionTime;
		
		String Innetwork=rs_claims.getString("HASSGN");
		String ClaimNumber=rs_claims.getString("HCLMNO");
		
		Double OOPMAX=rs_medpcr.getDouble("PINON");
		Double DEDMAX=rs_medpcr.getDouble("PINDD");
		
		Double OOPFINAL=0.0;
		Double DEDFINAL=0.0;
		
		Double RemainingBalance1=DEDMAX-rs_medacc.getDouble("AANNDE");
		Double RemainingBalance2=OOPMAX-rs_medacc.getDouble("OANNDE");
		
		Double AccumBenPeriod1=0.0;
		Double AccumBenPeriod2=0.0;
		String AccumBalance1=rs_medacc.getString("AANNDE");
		String AccumBalance2=rs_medacc.getString("OANNDE");
		
		
		String Remaining1="";
		String Remaining2="";
		
		String RelCodeInbound;
		String RelCode = "";
		Date DOBDATE=new Date();
		Date DEPDOBDATE=new Date();
		
		
		String DOB;
		String DOBEE = "";
		String DOBSP = "";
		String DepDOB = "";
		String FinalDOB = "";

		String INSUREFIRST;
		String INSURELAST;
		
		String INSDEPFIRST = "";
		String INSDEPLAST = "";

		String FINALFIRST;
		String FINALLAST;

		String STATE;

		String INSURESEX;
		String INSDEPSEX = "";
		
		String FinalSex;
		String Copay="";
		String DAMTCC = "";
		String DAMTC2 = "";
		String DAMTC3 = "";
		Double DAMTCF;

		String DAMTDD = "";
		String DAMTD2 = "";
		String DAMTD3 = "";
		Double DAMTDF;
		
		String NetworkIndicator1="";
		String NetworkIndicator2="0";
		String AccumBalanceQualifier1="";
		String AccumBalanceQualifier2="";
		String ActionCode1Applied="";
		String ActionCode2Applied="";
		Double AppliedAmount1=0.0;
		Double AppliedAmount2=0.0;
		
		String Applied1="";
		String Applied2="";
		String HDOSpre=rs_claims.getString("HDOS");
		String HDOS="";
		
		String exgrpno=rs_clmnot.getString("NCNOT1");
		SimpleDateFormat inputFormat = new SimpleDateFormat("MMddyyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat inputFormat2 = new SimpleDateFormat("MMddyy");
		Date HDOSdate=new Date();
		try {
			if(HDOSpre.length()==5){
				HDOSpre="0"+HDOSpre;
			}
			
			HDOSdate=inputFormat2.parse(HDOSpre);
			HDOS=outputFormat.format(HDOSdate);
			
			System.out.println("HDOSpre: "+HDOSpre+" || HDOS post: "+HDOS);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(isEE){

		RelCodeInbound = rs_claims.getString("HDEP");
		
		DOB=rs_insure.getString("IDOB");
		
		if(DOB.length()==7)
			DOB="0"+DOB;
		try {
			
			 DOBEE= outputFormat.format(inputFormat.parse(DOB));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		INSUREFIRST=rs_insure.getString("IFNAM");
		INSURELAST=rs_insure.getString("ILNAM");

		FINALFIRST="";
		FINALLAST="";

		STATE=rs_insure.getString("ISTATE");

		INSURESEX=rs_insure.getString("ISEX");
		
		FinalSex="";
		Copay=rs_claims.getString("DCOPAY");
		DAMTCC = rs_claims.getString("DAMTCC");
		DAMTC2=rs_claims.getString("DAMTC2");
		DAMTC3 = rs_claims.getString("DAMTC3");
		DAMTCF=Double.valueOf(DAMTCC)+Double.valueOf(DAMTC2)+Double.valueOf(DAMTC3)+Double.valueOf(Copay);

		DAMTDD = rs_claims.getString("DAMTDD");
		DAMTD2=rs_claims.getString("DAMTD2");
		DAMTD3 = rs_claims.getString("DAMTD3");
		DAMTDF=Double.valueOf(DAMTDD)+Double.valueOf(DAMTD2)+Double.valueOf(DAMTD3);
		}
		else{
			
			
			RelCodeInbound = rs_claims.getString("HDEP");
			
			DOB=rs_insure.getString("IDOB");
			DepDOB=rs_insure.getString("DDOB");
			if(DepDOB.length()==7)
				DepDOB="0"+DepDOB;
			try {
				 DOBSP= outputFormat.format(inputFormat.parse(DepDOB));
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			
			INSUREFIRST=rs_insure.getString("IFNAM");
			INSURELAST=rs_insure.getString("ILNAM");

			INSDEPFIRST=rs_insure.getString("DFNAM");
			INSDEPLAST=rs_insure.getString("DLNAM");

			
			FINALFIRST="";
			FINALLAST="";

			STATE=rs_insure.getString("ISTATE");

			INSURESEX=rs_insure.getString("ISEX");
			INSDEPSEX=rs_insure.getString("DSEX");
			FinalSex="";
			
			Copay=rs_claims.getString("DCOPAY");
			DAMTCC = rs_claims.getString("DAMTCC");
			DAMTC2=rs_claims.getString("DAMTC2");
			DAMTC3 = rs_claims.getString("DAMTC3");
			DAMTCF=Double.valueOf(DAMTCC)+Double.valueOf(DAMTC2)+Double.valueOf(DAMTC3)+Double.valueOf(Copay);
			
			DAMTDD = rs_claims.getString("DAMTDD");
			DAMTD2=rs_claims.getString("DAMTD2");
			DAMTD3 = rs_claims.getString("DAMTD3");
			DAMTDF=Double.valueOf(DAMTDD)+Double.valueOf(DAMTD2)+Double.valueOf(DAMTD3);
		}
		
		
		if(RelCodeInbound.equals("EE"))
		{
			RelCode="1";
			FinalDOB=DOBEE;
			FINALFIRST=INSUREFIRST;
			FINALLAST=INSURELAST;
			FinalSex=INSURESEX;
			if(FinalSex.equals("F")){
				FinalSex="2";
			}
			if(FinalSex.equalsIgnoreCase("M")){
				FinalSex="1";
			}
			
		}

		if(RelCodeInbound.equals("SP"))
		{
			FinalDOB=DOBSP;
			RelCode="2";
			FINALFIRST=INSDEPFIRST;
			FINALLAST=INSDEPLAST;
			FinalSex=INSDEPSEX;
			if(FinalSex.equals("F")){
				FinalSex="2";
			}
			if(FinalSex.equalsIgnoreCase("M")){
				FinalSex="1";
			}
			
		}

		if(!RelCodeInbound.equals("EE") && (!RelCodeInbound.equals("SP")))
		{
			FinalDOB=DOBSP;
			RelCode="3";
			FINALFIRST=INSDEPFIRST;
			FINALLAST=INSDEPLAST;
			FinalSex=INSDEPSEX;
			if(FinalSex.equals("F")){
				FinalSex="2";
			}
			if(FinalSex.equalsIgnoreCase("M")){
				FinalSex="1";
			}
			
		}	
		
		if(DAMTCF!=0.0)
		{
			if(DAMTCF>0)
			{
				ActionCode1Applied="+";
			}
			
			if(DAMTCF<0)
			{
				ActionCode1Applied="-";
			}
			
			NetworkIndicator1="1";
			AccumBalanceQualifier1="04";
			AppliedAmount1=DAMTCF;
			Applied1=Double.toString(AppliedAmount1);
			DEDFINAL=Double.valueOf(DEDMAX)-DAMTCF;
			Remaining1=Double.toString(RemainingBalance1);
			if(DAMTDF!=0.0)
			{
				if(DAMTDF>0){
					ActionCode2Applied="+";
				}
				if(DAMTDF<0){
					ActionCode2Applied="-";
					
					
				}
				
				NetworkIndicator2="2";
				AccumBalanceQualifier2="05";	
				AppliedAmount2=DAMTDF;
				Applied2=Double.toString(AppliedAmount2);
				OOPFINAL=Double.valueOf(OOPMAX)-DAMTDF;
				Remaining2=Double.toString(RemainingBalance2);
			}
			
			if(DAMTDF==0.0)
			{
				NetworkIndicator2="0";
				AccumBalanceQualifier2=" ";	
				Applied2="00";
			}
				
		}
		
		if((DAMTCF==0.0)&&(DAMTDF!=0.0))
		{
			if(DAMTDF>0)
			{
				ActionCode1Applied="+";
			}
			
			if(DAMTDF<0)
			{
				ActionCode1Applied="-";
			}
			
			NetworkIndicator1="2";
			AccumBalanceQualifier1="05";
			Applied2="00";
			AppliedAmount1=DAMTDF;
			Applied1=Double.toString(AppliedAmount1);
			OOPFINAL=Double.valueOf(OOPMAX)-DAMTDF;
			Remaining1=Double.toString(RemainingBalance1);
			Remaining2=Double.toString(RemainingBalance2);
		}
		
		if(AppliedAmount2==0)
		{
			Remaining2="0";
		}
		
	Applied1=Applied1.replace(".","");
	Applied2=Applied2.replace(".","");
	Applied1=Applied1.replace("-","");
	Applied2=Applied2.replace("-","");
	Remaining1=Remaining1.replace(".","");
	Remaining2=Remaining2.replace(".","");
	Remaining1=Remaining1.replace("-","");
	Remaining2=Remaining2.replace("-","");
	AccumBalance1=AccumBalance1.replace(".","");
	AccumBalance2=AccumBalance2.replace(".","");
	AccumBalance1=AccumBalance1.replace("-","");
	AccumBalance2=AccumBalance2.replace("-","");
	
	System.out.println("RelCode Inbound  "+FINALFIRST);
	
	
		if(Applied1.length()<10){
			while(Applied1.length()<10){
				Applied1="0"+Applied1;
			}
		}
		
		if(Applied2.length()<10){
			while(Applied2.length()<10){
				Applied2="0"+Applied2;
			}
		}
		
		if(Remaining1.length()<10){
			while(Remaining1.length()<10){
				Remaining1="0"+Remaining1;
			}
		}
		
		if(Remaining2.length()<10){
			while(Remaining2.length()<10){
				Remaining2="0"+Remaining2;
			}
		}
		if(AccumBalance1.trim().length()<10){
			while(AccumBalance1.trim().length()<10){
				AccumBalance1="0"+AccumBalance1.trim();
			}
		}
		if(AccumBalance2.trim().length()<10){
			while(AccumBalance2.trim().length()<10){
				AccumBalance2="0"+AccumBalance2.trim();
			}
		}
		
		/*
		 * data output
		 */
		
		String[] object = new String[]{
				"",
				"DT",//Record Type
				"DQ",//Transmission File
				"10",//Version/Release
				"BAC",//Sender ID
				"MHS",//Reciever ID 
				"0000",//Submission Number
				"A",//Transaction Response Status
				"000",//Reject code
				"01700",//Record Length
				"",//Reserved
				TransmissionDate.toString(),//"Todays Date"
				TransmissionTime.toString(),//"HHMMSS.DDD"
				HDOS,//DateOfService
				"",//Service Provider ID Qualifier
				"",//Service Provider ID
				"04",//Document Reference Identifier Qualifier
				"999999999999999",//Document Reference Identifer ???????
				TransmissionID.toString(),//Transmission ID
				"9",//Benefit Type
				"1",//In Network Indicator
				"0",//Formulary Status(default to Spaces)
				"00",//Accumulator Action Code
				"",//Sender Reference Number
				"",//Insurance Code
				"",//Accumulator Balance Benefit Type
				"",//Benefit Effective Date
				"",//Benefit Termination Date
				"",//Accumulator Change Source Code
				exgrpno.trim(),//Transaction ID
				"0000",//Transaction ID Cross Reference
				"",//Adujstment Reason Code
				"",//Accumulator Reference Time Stamp
				"",//Reserved
				hssn,//CardHolder ID from Express Scripts
				"KRMA",//Group Number
				FINALFIRST,//First Name from Claim
				"",//Middle Initial
				FINALLAST,//Last Name from Claim
				RelCode,//Relationship Code								
				FinalDOB,//DOB
				FinalSex,//Patient Gender Code
				STATE,//State
				"",//CardHolder Last Name
				" ",//Carrier Number(Default to Spaces)
				" ",//Contract Number(Default to Spaces) 
				" ",//Client Pass Through(Default to Spaces)
				"",//Family ID(Spaces)
				"",//Card Holder ID(Alt)(Spaces)
				"",//Group ID(Alt)(Spaces)
				"",//Patient ID(Spaces)
				"",//Person Code
				"",//Reserved
				"01",//Accumulator Balance Count
				"",//Accumlator Specific Category Type
				"",//Reserved
				AccumBalanceQualifier1,//Accumlator Balance Qualifier 1
				NetworkIndicator1 ,//Accumlator Network Indicator 1
				Applied1,//Accumulator Applied Amount 1
				ActionCode1Applied,//ActionCode1
				AccumBalance1.trim(),//Accumulator Benefit Period 1
				ActionCode1Applied,//ActionCode1
				Remaining1,//Accumulator Remaining Balance1
				"+",//ActionCode1
				"05",//Accumlator Balance Qualifier 2
				NetworkIndicator2,//Accumlator Network Qualifer 2
				Applied2,//Accumulator Applied Amount 2
				"+",//ActionCode 2
				AccumBalance2.trim(),//Accumulator Benefit Period 2
				"+",//ActionCode 2
				Remaining2,//Accumulator Remaining Balance 2
				"+",//ActionCode 2
				"0000000000",//Accumlator Balance Qualifier 3
				" ",//Accumlator Network Indicator 3
				"0000000000",//Accumulator Applied Amount 3
				"+",//ActionCode3
				"0000000000",//Accumulator Benefit Period 3
				"+",//ActionCode3
				"0000000000",//Accumulator Remaining Balance3
				"+",//ActionCode3
				"0000000000",//Accumlator Balance Qualifier 4
				"",//Accumlator Network Indicator 4
				"0000000000",//Accumulator Applied Amount 4
				"+",//ActionCode4
				"0000000000",//Accumulator Benefit Period 4
				"+",//ActionCode4
				"0000000000",//Accumulator Remaining Balance4
				"+",//ActionCode4
				"0000000000",//Accumlator Balance Qualifier 5
				"",//Accumlator Network Qualifer 5
				"0000000000",//Accumulator Applied Amount 5
				"+",//ActionCode5
				"0000000000",//Accumulator Benefit Period 5
				"+",//ActionCode5
				"0000000000",//Accumulator Remaining Balance5
				"+",//ActionCode5
				"0000000000",//Accumlator Balance Qualifier 6
				" ",//Accumlator Network Qualifer 6
				"0000000000",//Accumulator Applied Amount 6
				"+",//ActionCode6
				"0000000000",//Accumulator Benefit Period 6
				"+",//ActionCode6
				"0000000000",//Accumulator Remaining Balance6
				"+",//ActionCode6
				"",//Reserved default to 24 spaces
				"0000000000",//Accumlator Balance Qualifier 7
				" ",//Accumlator Network Qualifer 7
				"0000000000",//Accumulator Applied Amount 7
				"+",//ActionCode7
				"0000000000",//Accumulator Benefit Period 7
				"+",//ActionCode7
				"0000000000",//Accumulator Remaining Balance7
				"+",//ActionCode7
				"0000000000",//Accumlator Balance Qualifier 8
				" ",//Accumlator Network Qualifer 8
				"0000000000",//Accumulator Applied Amount 8
				"+",//ActionCode8
				"0000000000",//Accumulator Benefit Period 8
				"+",//ActionCode8
				"0000000000",//Accumulator Remaining Balance8
				"+",//ActionCode8
				"0000000000",//Accumlator Balance Qualifier 9
				" ",//Accumlator Network Qualifer 9
				"0000000000",//Accumulator Applied Amount 9
				"+",//ActionCode9
				"0000000000",//Accumulator Benefit Period 9
				"+",//ActionCode9
				"0000000000",//Accumulator Remaining Balance9
				"+",//ActionCode9
				"0000000000",//Accumlator Balance Qualifier 10
				" ",//Accumlator Network Qualifer 10
				"0000000000",//Accumulator Applied Amount 10
				"+",//ActionCode10
				"0000000000",//Accumulator Benefit Period 10
				"+",//ActionCode10
				"0000000000",//Accumulator Remaining Balance10
				"+",//ActionCode10
				"0000000000",//Accumlator Balance Qualifier 11
				" ",//Accumlator Network Qualifer 11
				"0000000000",//Accumulator Applied Amount 11
				"+",//ActionCode11
				"0000000000",//Accumulator Benefit Period 11
				"+",//ActionCode11
				"0000000000",//Accumulator Remaining Balance11
				"+",//ActionCode11
				"0000000000",//Accumlator Balance Qualifier 12
				" ",//Accumlator Network Qualifer 12
				"0000000000",//Accumulator Applied Amount 12
				"+",//ActionCode12
				"0000000000",//Accumulator Benefit Period 12
				"+",//ActionCode12
				"0000000000",//Accumulator Remaining Balance12
				"+",//ActionCode12
				"",//Optional Data Indicator
				"",//Total Amount Paid
				"",//Action Code 1
				"",//Amount of Copay
				"",//Action Code 2
				"",//Paitent Pay Amount 
				"",//Action Code 3
				"",//Amount Attributed to Product selection
				"",//Action Code 4
				"",//Amount Attributed to Sales Tax
				"",//Action Code 5
				"",// Amount Attributed to Processor Fee
				"",//Action Code 6
				"",//Gross Amount Due
				"",//Action Code 7
				"",//Invoiced Amount
				"",//Action Code 8
				"",//Penalty Amount
				"",//Action Code 9
				"",//Reserved
				"",//Product/Service Id Qualifier
				"",//Product/Service Id
				"",//Days Supply
				"",//Quantity Dispense
				"",//Product / Service Name (NDC NUMBER)
				"",//Brand / Generic Indicator
				"",//Therapeutic Class Code Qualifier
				"",//Therapeutic Class Code - Specific
				"",//Dispense As Written (Daw)/Product Selection
				"" //Reserved

		};
		System.out.println("Total For Co-Insurance: "+DAMTCF+" || Total For Deductable: "+DAMTDF);
		writeFile(object);

		return object;
	}

@SuppressWarnings("unused")
public static String findplan(String hgrpno,String hssn){
	
	if (conn==null){

		//Conection to database
		String hostname="";//getting the host name of the computer that we are using 
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		}catch (UnknownHostException ex){System.out.println("Hostname can not be resolved");}


		props = new Properties();//get DB info
		try {
			if (hostname.equals("Hi-TechHealthIn")) 
				props.load(new FileInputStream("C:/Users/Hi-TechHealth Inc/git/Hi-Tech-Health/mydb2.properties"));
			else
				props.load(new FileInputStream("/java/mydb2.properties"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}


		final String DRIVER = "com.ibm.as400.access.AS400JDBCDriver"; 
		final String URL = "jdbc:as400://"+props.getProperty("local_system").trim()+"/"+LIB+";naming=system";//jdbc:db2

		try {
			Class.forName(DRIVER); //making the connection
			conn = DriverManager.getConnection(URL, props.getProperty("userId").trim(), props.getProperty("password").trim()); 

			conn.createStatement().execute("CREATE ALIAS QTEMP/INSURE_"+CLIENT+" FOR "+LIB+"/INSURE("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/INSDEP_"+CLIENT+" FOR "+LIB+"/INSDEP("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/CLMDET_"+CLIENT+" FOR "+LIB+"/CLMDET("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/CLMHDR_"+CLIENT+" FOR "+LIB+"/CLMHDR("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/MEDPCR_"+CLIENT+" FOR "+LIB+"/MEDPCR("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/MEDACC_"+CLIENT+" FOR "+LIB+"/MEDACC("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/GRPMST_"+CLIENT+" FOR "+LIB+"/GRPMST("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/BLCKPLN_"+CLIENT+" FOR "+LIB+"/BLCKPLN("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/CLMNOT_"+CLIENT+" FOR "+LIB+"/CLMNOT("+CLIENT+")");

			System.out.println("Connected to database");
		} catch (ClassNotFoundException e) {e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

	//START of the finding Plan method
	
	
		String planname="";//GPL1-50
		String gcarr="";//value to go to blckpln with
		String plansenrolled="";
		String IINO="";
		int finalplans;
		System.out.println("Social: "+hssn);
	try {
		ResultSet rs=null;//Result set for plan names
		int rsblck;//result set for Block name
		ResultSet rsinsure=null;//result set for insure
		
		 rs = DBqueryRS("SELECT * FROM QTEMP/GRPMST_"+CLIENT+" WHERE GGRPNO = '"+hgrpno+"'", rs);
		 rsinsure = DBqueryRS("SELECT * FROM QTEMP/INSURE_"+CLIENT+" WHERE ISSN = '"+hssn+"'", rsinsure);
		 System.out.println(hssn);
		 rsinsure.next();
		 rs.next();
		
			 for(int i=01;i<50;i++)
			 {
				 
				 if(i<10)
				 {
				 plansenrolled=Integer.toString(i);
				 plansenrolled="0"+plansenrolled;
				 }
				 else{
					 plansenrolled=Integer.toString(i);
				 }
				 
				 IINO=rsinsure.getString("IINO"+plansenrolled).trim();
				
		 if(!rs.getString("GPL"+i).trim().equals("")){
					planname=rs.getString("GPL"+i).trim();
					gcarr=rs.getString("GCARR");
					
				if(!IINO.equals(""))
					 {
						 System.out.println("IINO"+plansenrolled+" is "+IINO+ "in plan:  "+planname);
						 finalplans=i;
						 
						rsblck = DBqueryINT("SELECT COUNT(*) FROM QTEMP/BLCKPLN_"+CLIENT+" WHERE CPNBR = '"+gcarr+"' AND CPPLN='"+planname+"' AND CCOVRG='R'");
						System.out.println("Count of plan"+rsblck);
						if(rsblck==1){
							
					 }
					}
						
				 }
		 }
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return planname;
}
private static int DBqueryINT(String query){
		int result=-1;
		try {
			Statement stmt1 = conn.createStatement();
			ResultSet rs = stmt1.executeQuery(query);
			
			rs.next();
			result = rs.getInt(1);
			  	        
			rs.close();		
			stmt1.close();
		} catch (SQLException e) {e.printStackTrace();}
 	return result;
 }
	private static void writeFile(String[] aBook) {

		lengths = new int[]{200,2,2,2,30,30,4,1,3,5,20,8,8,8,2,15,
				2,15,50,1,1,1,2,30,20,1,8,8,1,30,30,1,26,13,20,15,25,1,
				35,1,8,1,2,35,9,15,50,20,20,15,20,3,90,2,2,20,2,1,10,1,
				10,1,10,1,2,1,10,1,10,1,10,1,2,1,10,1,10,1,10,1,2,1,10,
				1,10,1,10,1,2,1,10,1,10,1,10,1,2,1,10,1,10,1,10,1,24,2,
				1,10,1,10,1,10,1,2,1,10,1,10,1,10,1,2,1,10,1,10,1,10,1,
				2,1,10,1,10,1,10,1,2,1,10,1,10,1,10,1,2,1,10,1,10,1,10,
				1,1,10,1,10,1,10,1,10,1,10,1,10,1,10,1,10,1,10,1,23,2,
				19,3,10,30,1,1,17,1,48};


		for (int j=0; j<lengths.length; j++){
			if (aBook[j].length()>lengths[j]){}

			finalString.append(aBook[j]);
			for (int g=aBook[j].length(); g<lengths[j]; g++){
				finalString.append(" ");
			}
		}
		finalString.append("\r\n");
	}

	@SuppressWarnings("unused")
	private static String DBquery(String query, String file){	
		Statement stmt = null;
		String result="";

		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) 
				result = rs.getString(1);

			rs.close();				
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static void connect(){


		System.currentTimeMillis();	 
		System.out.println("Connecting to the Database");	 
		String hostname="";//getting the host name of the computer that we are using 
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		}catch (UnknownHostException ex){System.out.println("Hostname can not be resolved");}


		props = new Properties();//get DB info
		try {
			if (hostname.equals("Hi-TechHealthIn")) 
				props.load(new FileInputStream("C:/Users/Hi-TechHealth Inc/git/Hi-Tech-Health/mydb2.properties"));
			else
				props.load(new FileInputStream("/java/mydb2.properties"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		
		final String DRIVER = "com.ibm.as400.access.AS400JDBCDriver"; 
		final String URL = "jdbc:as400://"+props.getProperty("local_system").trim()+"/"+LIB+";naming=system";//jdbc:db2

		try {
			Class.forName(DRIVER); //making the connection
			conn = DriverManager.getConnection(URL, props.getProperty("userId").trim(), props.getProperty("password").trim()); 

			conn.createStatement().execute("CREATE ALIAS QTEMP/INSURE_"+CLIENT+" FOR "+LIB+"/INSURE("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/INSDEP_"+CLIENT+" FOR "+LIB+"/INSDEP("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/CLMDET_"+CLIENT+" FOR "+LIB+"/CLMDET("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/CLMHDR_"+CLIENT+" FOR "+LIB+"/CLMHDR("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/MEDPCR_"+CLIENT+" FOR "+LIB+"/MEDPCR("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/MEDACC_"+CLIENT+" FOR "+LIB+"/MEDACC("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/GRPMST_"+CLIENT+" FOR "+LIB+"/GRPMST("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/BLCKPLN_"+CLIENT+" FOR "+LIB+"/BLCKPLN("+CLIENT+")");
			conn.createStatement().execute("CREATE ALIAS QTEMP/CLMNOT_"+CLIENT+" FOR "+LIB+"/CLMNOT("+CLIENT+")");
			
			System.out.println("Connected to database");
			
		} catch (ClassNotFoundException e) {e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

	}

private static ResultSet DBqueryRS(String query, ResultSet rs){
 		try {
 			Statement stmt1 = conn.createStatement();
 			rs = stmt1.executeQuery(query);
 			
 			//stmt1.close();
 		} catch (SQLException e) {e.printStackTrace();}
     	return rs;
     } 

private static void DBinsert(String query, String file){	
	 	Statement stmt = null;
					
    	 try {
			 	stmt = conn.createStatement();
			 	stmt.execute(query);
			 			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
    	
    }
	
@SuppressWarnings("unused")
public static String CallCL(){
	
	
		String data="";
		 String clmnbr="       ";
		 String commandStr = "CALL TESTLIB/GETCLMC PARM('BW1' '"+clmnbr+"')";
		  String hostname="";//getting the host name of the computer that we are using 
			try {
				hostname = InetAddress.getLocalHost().getHostName();
			}catch (UnknownHostException ex){System.out.println("Hostname can not be resolved");}


			props = new Properties();//get DB info
			try {
				if (hostname.equals("Hi-TechHealthIn")) 
					props.load(new FileInputStream("C:/Users/Hi-TechHealth Inc/git/Hi-Tech-Health/mydb2.properties"));
				else
					props.load(new FileInputStream("/java/mydb2.properties"));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		  AS400 as400 = null;
		  
		  try  {
		   // Create an AS400 object  
		   as400 = new AS400(props.getProperty("local_system").trim(), props.getProperty("userId").trim(), props.getProperty("password").trim());  
		   
		   QSYSObjectPathName path = new QSYSObjectPathName("TESTDATA", "CLMNBR", "DTAARA");
		   CharacterDataArea dataArea = new CharacterDataArea(as400, path.getPath());
		   
		   data = dataArea.read();
		   
		   // Create a Command object
		   CommandCall command = new CommandCall(as400);
		 
		   //Run the command.
		  //System.out.println("Executing: " + commandStr);
		  boolean success = command.run(commandStr);
		    
		   if (success) {  
		    System.out.println("Command Executed Successfully.\n");
		   }else{
		    System.out.println("Command Failed!");
		   }
		    
		   // Get the command results
		  AS400Message[] messageList = command.getMessageList();
		   for (AS400Message message : messageList){
		    //System.out.println(message.getText());
		   }
		  } catch (Exception e) {  
		   e.printStackTrace();  
		  }finally{
		   try{
		    // Make sure to disconnect   
		    as400.disconnectAllServices();  
		   }catch(Exception e){}
		  } 
		  
		  return data;
		 }  
		


}
